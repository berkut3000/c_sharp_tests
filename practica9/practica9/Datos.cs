﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 09/03/2017
 * Time: 03:17 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
	using System.Data;

namespace practica9
{
	/// <summary>
	/// Description of Datos.
	/// </summary>
	public partial class Datos : Form
	{
		MySqlConnection cnn = new MySqlConnection();
		string cadenaConexion = null;
		public Datos()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void DatosLoad(object sender, EventArgs e)
		{
	
		}
		public void conectar()
		{
			try
			{
				cadenaConexion = "server=localhost; database=baseprogramacion; uid=root; pwd=pasito perron";
				cnn.ConnectionString = cadenaConexion;
				cnn.Open();
				//MessageBox.Show("Conexión a la base de datos correcta");
			}catch(Exception ex)
			{
				MessageBox.Show("Error  de conexión a la base de datos");
			}
		}
		void mostrarTabla()
		{
			MySqlCommand cmd = cnn.CreateCommand();
			cmd.CommandType= System.Data.CommandType.Text;
			//refresh the table
			cmd.CommandText = "select * from books";
			MySqlDataReader leer;
			leer = cmd.ExecuteReader();
			this.dataGridView1.Rows.Clear();
			while(leer.Read())
			{
				this.dataGridView1.Rows.Add(leer.GetValue(0),leer.GetValue(1),leer.GetValue(2),leer.GetValue(3),leer.GetValue(4));
			}
			cnn.Close();
		}
		void insertar()
		{
			MySqlCommand cmd = cnn.CreateCommand();
			cmd.CommandType= System.Data.CommandType.Text;
			cmd.CommandText= "insert into books (NombreDeLibro, autor, ano, edicion) values ('"+NombreTB.Text+"', '"+AutorTB.Text+"', '"+AnoTB.Text+"', '"+EdicionTB.Text+"')";			
			cmd.ExecuteNonQuery();
			//cnn.Close();
		}
		void actualizar()
		{
			MySqlCommand cmd = cnn.CreateCommand();
			cmd.CommandType= System.Data.CommandType.Text;
			cmd.CommandText= "update books set NombreDeLibro = '"+NombreTB.Text+"', autor = '"+AutorTB.Text+"', ano = '"+AnoTB.Text+"', edicion = '"+EdicionTB.Text+"' where id = '"+IdTB.Text+"'";			
			cmd.ExecuteNonQuery();
		}
		void borrar()
		{
			MySqlCommand cmd = cnn.CreateCommand();
				cmd.CommandType= System.Data.CommandType.Text;
				//refresh the table
				cmd.CommandText = "delete from books where id ='"+IdTB.Text+"'";
				cmd.ExecuteNonQuery();
		}
		void buscar()
		{
			MySqlCommand cmd = cnn.CreateCommand();
			cmd.CommandType= System.Data.CommandType.Text;
			//cmd.CommandText= "select * from books where id like '"+IdTextBox.Text+"' ";
//			cmd.CommandText= "select * from books where id = '"+IdTextBox.Text+"' or NombreDeLibro like '%"+nombreTextBox.Text+"%' or autor = '"+autorTextBox.Text+"' or ano = '"+anoTextBox.Text+"' or edicion = '"+edicionTextBox.Text+"'    ";
			cmd.CommandText= "select * from books where id = '"+IdTB.Text+"' or NombreDeLibro = '"+NombreTB.Text+"' or autor = '"+AutorTB.Text+"' or ano = '"+AnoTB.Text+"' or edicion = '"+EdicionTB.Text+"'    ";
			MySqlDataReader leer;
			leer = cmd.ExecuteReader();
			this.dataGridView1.Rows.Clear();
				while(leer.Read())
				{
					this.dataGridView1.Rows.Add(leer.GetValue(0),leer.GetValue(1),leer.GetValue(2),leer.GetValue(3),leer.GetValue(4));
				}
				cnn.Close();
		}
		void CerrarClick(object sender, EventArgs e)
		{
			MainForm principal = new MainForm();
			this.Close();
			principal.Show();
		}
		void resetear()
		{
			IdTB.Text = "";
			NombreTB.Text = "";
			AutorTB.Text = "";
			AnoTB.Text = "";
			EdicionTB.Text = "";
		}
		void DataGridView1MouseDoubleClick(object sender, MouseEventArgs e)
		{
			IdTB.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
			NombreTB.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
			AutorTB.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
			AnoTB.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
			EdicionTB.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
		}
		
		
		
		void MostrarBClick(object sender, EventArgs e)
		{
			conectar();
			mostrarTabla();			
		}
		void InsertarBClick(object sender, EventArgs e)
		{
			conectar();
			insertar();
			mostrarTabla();
			resetear();
		}
		void ActualizarBClick(object sender, EventArgs e)
		{
			conectar();
			actualizar();
			mostrarTabla();
			resetear();
		}
		void BorrarBClick(object sender, EventArgs e)
		{
			conectar();
			borrar();
			mostrarTabla();
			resetear();
		}
		void BuscarBClick(object sender, EventArgs e)
		{
			conectar();
			buscar();
			//mostrarTabla();
			resetear();
		}
		
		
		
		
		
	}
}
