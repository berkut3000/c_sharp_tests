﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 09/03/2017
 * Time: 03:17 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace practica9
{
	partial class Datos
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox IdTB;
		private System.Windows.Forms.Button Cerrar;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox NombreTB;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox AnoTB;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox AutorTB;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox EdicionTB;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button InsertarB;
		private System.Windows.Forms.Button ActualizarB;
		private System.Windows.Forms.Button BorrarB;
		private System.Windows.Forms.Button MostrarB;
		private System.Windows.Forms.Button BuscarB;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID;
		private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
		private System.Windows.Forms.DataGridViewTextBoxColumn Autor;
		private System.Windows.Forms.DataGridViewTextBoxColumn ANo;
		private System.Windows.Forms.DataGridViewTextBoxColumn Edicion;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.IdTB = new System.Windows.Forms.TextBox();
			this.Cerrar = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.NombreTB = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.AnoTB = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.AutorTB = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.EdicionTB = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.InsertarB = new System.Windows.Forms.Button();
			this.ActualizarB = new System.Windows.Forms.Button();
			this.BorrarB = new System.Windows.Forms.Button();
			this.MostrarB = new System.Windows.Forms.Button();
			this.BuscarB = new System.Windows.Forms.Button();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Autor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ANo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Edicion = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// IdTB
			// 
			this.IdTB.Location = new System.Drawing.Point(143, 27);
			this.IdTB.Name = "IdTB";
			this.IdTB.Size = new System.Drawing.Size(100, 20);
			this.IdTB.TabIndex = 0;
			// 
			// Cerrar
			// 
			this.Cerrar.Location = new System.Drawing.Point(795, 314);
			this.Cerrar.Name = "Cerrar";
			this.Cerrar.Size = new System.Drawing.Size(75, 23);
			this.Cerrar.TabIndex = 1;
			this.Cerrar.Text = "Cerrar";
			this.Cerrar.UseVisualStyleBackColor = true;
			this.Cerrar.Click += new System.EventHandler(this.CerrarClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(37, 27);
			this.label1.Name = "label1";
			this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.label1.Size = new System.Drawing.Size(100, 22);
			this.label1.TabIndex = 2;
			this.label1.Text = "Id";
			// 
			// NombreTB
			// 
			this.NombreTB.Location = new System.Drawing.Point(143, 53);
			this.NombreTB.Name = "NombreTB";
			this.NombreTB.Size = new System.Drawing.Size(100, 20);
			this.NombreTB.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(37, 53);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 22);
			this.label2.TabIndex = 2;
			this.label2.Text = "Nombre";
			// 
			// AnoTB
			// 
			this.AnoTB.Location = new System.Drawing.Point(143, 113);
			this.AnoTB.Name = "AnoTB";
			this.AnoTB.Size = new System.Drawing.Size(100, 20);
			this.AnoTB.TabIndex = 0;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(37, 113);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 22);
			this.label3.TabIndex = 2;
			this.label3.Text = "Ano";
			// 
			// AutorTB
			// 
			this.AutorTB.Location = new System.Drawing.Point(143, 79);
			this.AutorTB.Name = "AutorTB";
			this.AutorTB.Size = new System.Drawing.Size(100, 20);
			this.AutorTB.TabIndex = 0;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(37, 79);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 22);
			this.label4.TabIndex = 2;
			this.label4.Text = "Autor";
			// 
			// EdicionTB
			// 
			this.EdicionTB.Location = new System.Drawing.Point(143, 139);
			this.EdicionTB.Name = "EdicionTB";
			this.EdicionTB.Size = new System.Drawing.Size(100, 20);
			this.EdicionTB.TabIndex = 0;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(37, 139);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 22);
			this.label5.TabIndex = 2;
			this.label5.Text = "Edicion";
			// 
			// InsertarB
			// 
			this.InsertarB.Location = new System.Drawing.Point(37, 187);
			this.InsertarB.Name = "InsertarB";
			this.InsertarB.Size = new System.Drawing.Size(75, 23);
			this.InsertarB.TabIndex = 1;
			this.InsertarB.Text = "Insertar";
			this.InsertarB.UseVisualStyleBackColor = true;
			this.InsertarB.Click += new System.EventHandler(this.InsertarBClick);
			// 
			// ActualizarB
			// 
			this.ActualizarB.Location = new System.Drawing.Point(118, 187);
			this.ActualizarB.Name = "ActualizarB";
			this.ActualizarB.Size = new System.Drawing.Size(75, 23);
			this.ActualizarB.TabIndex = 1;
			this.ActualizarB.Text = "Actualizar";
			this.ActualizarB.UseVisualStyleBackColor = true;
			this.ActualizarB.Click += new System.EventHandler(this.ActualizarBClick);
			// 
			// BorrarB
			// 
			this.BorrarB.Location = new System.Drawing.Point(199, 187);
			this.BorrarB.Name = "BorrarB";
			this.BorrarB.Size = new System.Drawing.Size(75, 23);
			this.BorrarB.TabIndex = 1;
			this.BorrarB.Text = "Borrar";
			this.BorrarB.UseVisualStyleBackColor = true;
			this.BorrarB.Click += new System.EventHandler(this.BorrarBClick);
			// 
			// MostrarB
			// 
			this.MostrarB.Location = new System.Drawing.Point(280, 187);
			this.MostrarB.Name = "MostrarB";
			this.MostrarB.Size = new System.Drawing.Size(75, 23);
			this.MostrarB.TabIndex = 1;
			this.MostrarB.Text = "Mostrar";
			this.MostrarB.UseVisualStyleBackColor = true;
			this.MostrarB.Click += new System.EventHandler(this.MostrarBClick);
			// 
			// BuscarB
			// 
			this.BuscarB.Location = new System.Drawing.Point(37, 216);
			this.BuscarB.Name = "BuscarB";
			this.BuscarB.Size = new System.Drawing.Size(75, 23);
			this.BuscarB.TabIndex = 1;
			this.BuscarB.Text = "buscar";
			this.BuscarB.UseVisualStyleBackColor = true;
			this.BuscarB.Click += new System.EventHandler(this.BuscarBClick);
			// 
			// dataGridView1
			// 
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.ID,
			this.Nombre,
			this.Autor,
			this.ANo,
			this.Edicion});
			this.dataGridView1.Location = new System.Drawing.Point(361, 27);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(524, 281);
			this.dataGridView1.TabIndex = 3;
			this.dataGridView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.DataGridView1MouseDoubleClick);
			// 
			// ID
			// 
			this.ID.HeaderText = "ID";
			this.ID.Name = "ID";
			// 
			// Nombre
			// 
			this.Nombre.HeaderText = "Nombre";
			this.Nombre.Name = "Nombre";
			// 
			// Autor
			// 
			this.Autor.HeaderText = "Autor";
			this.Autor.Name = "Autor";
			// 
			// ANo
			// 
			this.ANo.HeaderText = "Ano";
			this.ANo.Name = "ANo";
			// 
			// Edicion
			// 
			this.Edicion.HeaderText = "Edicion";
			this.Edicion.Name = "Edicion";
			// 
			// Datos
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(897, 363);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.BuscarB);
			this.Controls.Add(this.MostrarB);
			this.Controls.Add(this.BorrarB);
			this.Controls.Add(this.ActualizarB);
			this.Controls.Add(this.InsertarB);
			this.Controls.Add(this.Cerrar);
			this.Controls.Add(this.AutorTB);
			this.Controls.Add(this.EdicionTB);
			this.Controls.Add(this.AnoTB);
			this.Controls.Add(this.NombreTB);
			this.Controls.Add(this.IdTB);
			this.Name = "Datos";
			this.Text = "Datos";
			this.Load += new System.EventHandler(this.DatosLoad);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
