﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 08/03/2017
 * Time: 08:42 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace practica9
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button exitBT;
		private System.Windows.Forms.Button loginBT;
		private System.Windows.Forms.Label userLabel;
		private System.Windows.Forms.Label passLabel;
		private System.Windows.Forms.TextBox userTB;
		private System.Windows.Forms.TextBox passTB;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.exitBT = new System.Windows.Forms.Button();
			this.loginBT = new System.Windows.Forms.Button();
			this.userLabel = new System.Windows.Forms.Label();
			this.passLabel = new System.Windows.Forms.Label();
			this.userTB = new System.Windows.Forms.TextBox();
			this.passTB = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// exitBT
			// 
			this.exitBT.Location = new System.Drawing.Point(181, 227);
			this.exitBT.Name = "exitBT";
			this.exitBT.Size = new System.Drawing.Size(75, 23);
			this.exitBT.TabIndex = 4;
			this.exitBT.Text = "Salir";
			this.exitBT.UseVisualStyleBackColor = true;
			this.exitBT.Click += new System.EventHandler(this.ExitBTClick);
			// 
			// loginBT
			// 
			this.loginBT.Location = new System.Drawing.Point(100, 227);
			this.loginBT.Name = "loginBT";
			this.loginBT.Size = new System.Drawing.Size(75, 23);
			this.loginBT.TabIndex = 3;
			this.loginBT.Text = "Ingresar";
			this.loginBT.UseVisualStyleBackColor = true;
			this.loginBT.Click += new System.EventHandler(this.LoginBTClick);
			// 
			// userLabel
			// 
			this.userLabel.Location = new System.Drawing.Point(12, 25);
			this.userLabel.Name = "userLabel";
			this.userLabel.Size = new System.Drawing.Size(100, 23);
			this.userLabel.TabIndex = 5;
			this.userLabel.Text = "Usuario";
			// 
			// passLabel
			// 
			this.passLabel.Location = new System.Drawing.Point(12, 69);
			this.passLabel.Name = "passLabel";
			this.passLabel.Size = new System.Drawing.Size(100, 23);
			this.passLabel.TabIndex = 5;
			this.passLabel.Text = "Password";
			// 
			// userTB
			// 
			this.userTB.Location = new System.Drawing.Point(156, 28);
			this.userTB.Name = "userTB";
			this.userTB.Size = new System.Drawing.Size(100, 20);
			this.userTB.TabIndex = 1;
			// 
			// passTB
			// 
			this.passTB.Location = new System.Drawing.Point(156, 66);
			this.passTB.Name = "passTB";
			this.passTB.Size = new System.Drawing.Size(100, 20);
			this.passTB.TabIndex = 6;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.Controls.Add(this.passTB);
			this.Controls.Add(this.userTB);
			this.Controls.Add(this.passLabel);
			this.Controls.Add(this.userLabel);
			this.Controls.Add(this.loginBT);
			this.Controls.Add(this.exitBT);
			this.Name = "MainForm";
			this.Text = "practica9";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
