﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 23/01/2017
 * Time: 08:25 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace metodos
{
	class Program
	{
		private string nombre;
		private string edad;
		//Metodo 1
		public void DoStuff()//Visibilidad/Tipo de Retorno/nombre/parametros
		{
			Console.WriteLine("Estoy haciendo algo.");
		}
		//Metodo 2
		public void bienvenida()
		{
			Console.WriteLine("Hola Porfirio");
		}
		//Metodo 3
		public static void progPar()
		{
			//Variables
			decimal w,x,y,z = 0;
			Console.WriteLine("Ingrear numero: ");
			
			// TODO: Implement Functionality Here
			
			//Console.Write("Press any key to continue . . . ");
			string v = Console.ReadLine();
			w = Convert.ToDecimal(v);
			//Console.WriteLine(w);
			x = w%2;
			if (x != 0)
			{
				Console.WriteLine("Numero Impar");
				x = w%3;
				if (x != 0)
				{
					z = w%5;
					if(z != 0)
					{
						Console.WriteLine("Número Primo");
					}
					else
					{
						if(w == 5)
						{
							Console.WriteLine("Número Primo");
						}
						else
						{
							Console.WriteLine("Número No Primo");
						}
					}
				}
				else
				{
					if(w == 3)
					{
						Console.WriteLine("Número Primo");
					}
					else
					{
						Console.WriteLine("Número No primo");
					}
				}
				
			}
			else
			{
				Console.WriteLine("Número Par");
				if (w == 2)
				{
					Console.WriteLine("Número Primo");
				}
				else
				{
					Console.WriteLine("Número No Primo");
				}
			}
			
			
			
			
			Console.ReadKey(true);
			Console.Clear();
		}
		//Metodo 4
		public void datos()
		{
			Console.WriteLine("Ingrese nombre");
			string nombre = Console.ReadLine();
			Console.WriteLine("Ingrese apellido");
			string apellido = Console.ReadLine();
			Console.WriteLine("Ingrese edad");
			string edad = Console.ReadLine();
			Console.WriteLine("Ingrese direccion");
			string address = Console.ReadLine();
			Console.Clear(); //Borra la pantalla
			Console.WriteLine("Nombre: {0}, Apellido: {1}, Edad: {2}, Direccion: {3}.",nombre,apellido,edad,address);
			Console.ReadKey(true);
			Console.Clear();
		}
		//Metodo 5, hipotenusa
		public void hipo()
		{
			Console.WriteLine("Ingrese el primer lado");
			double a = Convert.ToDouble(Console.ReadLine());
			Console.WriteLine("Ingrese el segundo lado");
			double b = Convert.ToDouble(Console.ReadLine());
			double c = Math.Sqrt((Math.Pow(a,2)+Math.Pow(b,2)));
			Console.WriteLine("La hipotenusa es {0}.",c);
			Console.ReadKey(true);
			Console.Clear();
		}
		public void inp()
		{
			Console.WriteLine("Ingrese los numeros");
		}
		//Metodo 6 Suma
		public static int sumNum(int n1, int n2)
		{
			int resultado = n1 + n2;
			return resultado;
		}
		//Metodo 7 resta
		public static int resNum(int n1, int n2)
		{
			int resultado = n1 - n2;
			return resultado;
		}
		//Metodo 8 multiplicacion
		public static int mulNum(int n1, int n2)
		{
			int resultado = n1 * n2;
			return resultado;
		}
		//Metodo 9 seleccionar accion
		public void sel()
		{
			Console.Clear();
			Console.WriteLine("Seleccionar accion.\n1. Suma\n2. Resta\n3. Multiplicacion\n4. Division");
		}
		//Metodo 10 Division
		public static int divNum(int n1, int n2)
		{
			int resultado = n1 / n2;
			return resultado;
		}
		//Programa principal
		public static void Main(string[] args)
		{
			double result = 0;
			Program mensaje = new Program(); //
			mensaje.bienvenida();
			mensaje.inp();
			int a = Convert.ToInt32(Console.ReadLine());
			int b = Convert.ToInt32(Console.ReadLine());
		rep:
			mensaje.sel();
			int c = Convert.ToInt32(Console.ReadLine());
			switch(c)
			{
				case 1:
					result = sumNum(a,b);
					break;
				case 2:
					result = resNum(a,b);
					break;
				case 3:
					result = mulNum(a,b);
					break;
				case 4:
					result = divNum(a,b);
					break;
				default:
					Console.WriteLine("Seleccion no valida");
					Console.ReadKey(true);
					goto rep;
					
			}
			//progPar();
			//Console.WriteLine("Hello World");
			//mensaje.DoStuff();
			//mensaje.datos();
			//mensaje.hipo();
			//int result = sumNum(a,b);
			//int result2 = resNum(a,b);
			//int result3 = mulNum(a,b);
			//Console.WriteLine("Suma: {0} Resta: {1} Multiplicacion: {2}.",result,result2,result3);
			Console.Clear();
			Console.WriteLine("El resultado es {0}", result);
			Console.ReadKey(true);
			
		}
		
		
		
	}
}