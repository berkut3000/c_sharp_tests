﻿/*
 * Created by Antonio.
 * User: hydro
 * Date: 18/01/2017
 * Time: 03:06 p.m.
 * Strings
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace strings
{
	class Program
	{
		public static void cadenas()
		{
			int n1,n2,n,comp, i;
			string firstName = "Bienvenidos a la juegosfera", lastName = "Mota",cName = "antonio";
			Console.WriteLine("Nombre: {0} || Apellido: {1}",firstName,lastName);
			Console.WriteLine("El primer caracter de la cadena es: {0}",firstName[0]);
			Console.WriteLine("Las primeras 5 letras de la cadena son: {0}",firstName.Substring(0,5));
			n = firstName.Length;
			Console.WriteLine("La longitud de la primera cadena es: {0}",n);
			Console.WriteLine("La palabra al revés es: ");
			
			for(i = n-1; i>=0;i--)
			{
				Console.Write(firstName[i]);//Imprimir la cadena "firstname" al reves
			}
			Console.WriteLine("\nPosicion de \"o\" en cadena: {0}",firstName.IndexOf("o"));
			Console.WriteLine("\nPosicion de \"o\" en cadena desde el otro extremo: {0}",firstName.LastIndexOf("o"));
			
			borrar();
			
			Console.WriteLine("Por favr, ingrese otro nombre");
			firstName = Console.ReadLine();
			Console.WriteLine("Nombre: {0} || Apellido: {1}",firstName,lastName);
			firstName = firstName.ToLower();//Forza los caracteres ingresados a mayusculas
			n = firstName.Length;
			comp = String.Compare(firstName,cName);
			string firstName2 = firstName.Insert(n,", Antonio");
			int m = firstName2.Length;
			Console.WriteLine(firstName2);//agrega nuevos datos a la cadena
			Console.WriteLine(m);
			firstName2 = firstName2.Remove(n,m-n);
			Console.WriteLine(firstName2);
			Console.WriteLine("las acadenas a comparar son {0} y {1} ",firstName, cName);
			if(comp == 0)//compara las palabras ingresadas
			{
				Console.WriteLine("Palaras Iguales: {0}",comp);
			}
			else
			{
				Console.WriteLine("Palabras distintas: {0}",comp);
			}
		}
		
		public static void reemplazarCadena()
		{
			string cadena = "Ey chavos";
			cadena = cadena.Replace("Ey","Hey");
			Console.WriteLine(cadena);
		}
		
		public static void splitsydelimitadores()
		{
			string ejemplo = "audi,ford honda.nissan";
			char [] delimitador = {',','.',' '};
			string [] ejemploPartido = ejemplo.Split(delimitador);
			int m = ejemploPartido.Length;
			for(int i = 0; i<m;i++)
			{
				Console.WriteLine("Parte {0}, es {1}",i,ejemploPartido[i]);
			}
		}
		
		public static void borrar()
		{
			Console.ReadKey(true);
			Console.Clear();
		}
		public static void numerosCadena()
		{
			//Ingresar numeros desde cadena
			int [] numeros;
			numeros = new int[5];
			for(int f = 0; f<5;f++)
			{
				Console.WriteLine("Ingresa el numero {0}: ",f);
				numeros[f] = int.Parse(Console.ReadLine());
			}
			borrar();
			//mostrar numeros
			for(int f = 0; f<5;f++)
			{
				Console.WriteLine(numeros[f]);
			}
			
			
		}
		
		public static void sumas()
		{
			Console.WriteLine("Suma de 2 números");
			Console.Write("Ingrese el primer número: ");
			int n1 = int.Parse(Console.ReadLine());
			Console.Write("Ingrese el segundo número: ");
			int n2 = int.Parse(Console.ReadLine());
			Console.WriteLine("La suma de ambos números es: " + (n1+n2));
		}
		public static void caracteresCadena()
		{
			int i;
			string [] caracteres = new string[5];
			for(i = 0; i<3; i++)
			{
				Console.WriteLine("Ingresa el numero 0: ");
				caracteres[i] = Console.ReadLine();
			}
			for(i = 0; i<3; i++)
			{
				Console.Write("{0} ",caracteres[i]);
			}
		}
		
		public static void rfc()
		{
			//Programa para obtener el RFC
			Console.WriteLine("Ingresar Apellido paterno");
			string ap = Console.ReadLine();
			borrar();
			Console.WriteLine("Ingresar Apellido materno");
			string am = Console.ReadLine();
			borrar();
			Console.WriteLine("Ingresar Nombre");
			string name = Console.ReadLine();
			borrar();
		ry:
			Console.WriteLine("Ingresar año de nacimiento");
			string year = Console.ReadLine();
			if(year.Length != 4)
			{
				Console.WriteLine("Numero Invalido");
				borrar();
				goto ry;
			}
			borrar();
		rm:
			Console.WriteLine("Ingresar mes de nacimiento con formato de 00");
			string mes = Console.ReadLine();
			if(mes.Length != 2)
			{
				Console.WriteLine("Numero Invalido");
				borrar();
				goto rm;
			}
			borrar();
		rd:
			Console.WriteLine("Ingresar dia de nacimiento con formato de 00");
			string dia = Console.ReadLine();
			if(dia.Length != 2)
			{
				Console.WriteLine("Numero Invalido");
				borrar();
				goto rd;
			}
			borrar();
			/*ap = "aguilar";
			am = "mota";
			name = "antonio";
			year = "1993";
			mes = "05";
			dia = "24";*/
			year = year.Remove(0,2);
			borrar();
			int ppv = 0;
			int la = ap.Length;
			for (int i =1; i<=la;i++)
			{
				if ((ap[i] == 'a') || (ap[i] == 'e') || (ap[i] == 'i') || (ap[i] == 'o') || (ap[i] == 'u'))
				    {
				    	ppv = i;
				    	break;
				    }
			}
			Console.WriteLine("Tu rfc es : {0}{1}{2}{3}{4}{5}{6}", ap[0],ap[ppv],am[0],name[0],year,mes,dia);
		}
		public static void Main(string[] args)
		{
			/*cadenas();
			borrar();
			reemplazarCadena();
			borrar();
			splitsydelimitadores();
			borrar();
			numerosCadena();
			caracteresCadena();*/
			rfc();
			borrar();
			//sumas();
			//borrar();
		}
	}
}