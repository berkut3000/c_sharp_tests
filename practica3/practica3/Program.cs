﻿/*
 * Created by Antonio.
 * User: hydro
 * Date: 16/01/2017
 * Time: 08:27 a.m.
 *
 * 
 * Programa para incrementar el valor de una variable 
 * enn uno usando un ciclo while
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace practica3
{
	class Program
	{
		public static void Main(string[] args)
		{
			int n = 1;
			int o = 2;
			int p = 4;
			while (n<101)
			{
				Console.WriteLine(n);
				n++;
			}
			/*Console.WriteLine("Hello World!");
			
			// TODO: Implement Functionality Here
			
			*/
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
			while (o < 101)
			{
				Console.WriteLine(o);
				o = o + 2 ;
			}
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
			while (p < 101)
			{
				Console.WriteLine(p);
				p = p + 4;
			}
			Console.Write("Fin del programa, baby . . . ");
			Console.ReadKey(true);
			
		}
	}
}