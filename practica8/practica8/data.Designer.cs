﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 07/03/2017
 * Time: 10:20 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace practica8
{
	partial class data
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label ID;
		private System.Windows.Forms.Label Nombre;
		private System.Windows.Forms.Label Autor;
		private System.Windows.Forms.Label Ano;
		private System.Windows.Forms.Label Edicion;
		private System.Windows.Forms.TextBox IdTextBox;
		private System.Windows.Forms.TextBox nombreTextBox;
		private System.Windows.Forms.TextBox autorTextBox;
		private System.Windows.Forms.TextBox anoTextBox;
		private System.Windows.Forms.TextBox edicionTextBox;
		private System.Windows.Forms.Button save;
		private System.Windows.Forms.Button search;
		private System.Windows.Forms.Button update;
		private System.Windows.Forms.Button cerrarDatos;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewTextBoxColumn C1;
		private System.Windows.Forms.DataGridViewTextBoxColumn C2;
		private System.Windows.Forms.DataGridViewTextBoxColumn C3;
		private System.Windows.Forms.DataGridViewTextBoxColumn C4;
		private System.Windows.Forms.DataGridViewTextBoxColumn C5;
		private System.Windows.Forms.Button mostrar;
		private System.Windows.Forms.Button Borrar;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.ID = new System.Windows.Forms.Label();
			this.Nombre = new System.Windows.Forms.Label();
			this.Autor = new System.Windows.Forms.Label();
			this.Ano = new System.Windows.Forms.Label();
			this.Edicion = new System.Windows.Forms.Label();
			this.IdTextBox = new System.Windows.Forms.TextBox();
			this.nombreTextBox = new System.Windows.Forms.TextBox();
			this.autorTextBox = new System.Windows.Forms.TextBox();
			this.anoTextBox = new System.Windows.Forms.TextBox();
			this.edicionTextBox = new System.Windows.Forms.TextBox();
			this.save = new System.Windows.Forms.Button();
			this.search = new System.Windows.Forms.Button();
			this.update = new System.Windows.Forms.Button();
			this.cerrarDatos = new System.Windows.Forms.Button();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.C1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.C2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.C3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.C4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.C5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.mostrar = new System.Windows.Forms.Button();
			this.Borrar = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// ID
			// 
			this.ID.Location = new System.Drawing.Point(32, 23);
			this.ID.Name = "ID";
			this.ID.Size = new System.Drawing.Size(100, 23);
			this.ID.TabIndex = 0;
			this.ID.Text = "ID";
			// 
			// Nombre
			// 
			this.Nombre.Location = new System.Drawing.Point(32, 68);
			this.Nombre.Name = "Nombre";
			this.Nombre.Size = new System.Drawing.Size(100, 23);
			this.Nombre.TabIndex = 0;
			this.Nombre.Text = "Nombre";
			// 
			// Autor
			// 
			this.Autor.Location = new System.Drawing.Point(32, 116);
			this.Autor.Name = "Autor";
			this.Autor.Size = new System.Drawing.Size(100, 23);
			this.Autor.TabIndex = 0;
			this.Autor.Text = "Autor";
			// 
			// Ano
			// 
			this.Ano.Location = new System.Drawing.Point(32, 155);
			this.Ano.Name = "Ano";
			this.Ano.Size = new System.Drawing.Size(100, 23);
			this.Ano.TabIndex = 0;
			this.Ano.Text = "Año";
			// 
			// Edicion
			// 
			this.Edicion.Location = new System.Drawing.Point(32, 197);
			this.Edicion.Name = "Edicion";
			this.Edicion.Size = new System.Drawing.Size(100, 23);
			this.Edicion.TabIndex = 0;
			this.Edicion.Text = "Edicion";
			// 
			// IdTextBox
			// 
			this.IdTextBox.Location = new System.Drawing.Point(126, 20);
			this.IdTextBox.Name = "IdTextBox";
			this.IdTextBox.Size = new System.Drawing.Size(100, 20);
			this.IdTextBox.TabIndex = 1;
			// 
			// nombreTextBox
			// 
			this.nombreTextBox.Location = new System.Drawing.Point(126, 65);
			this.nombreTextBox.Name = "nombreTextBox";
			this.nombreTextBox.Size = new System.Drawing.Size(100, 20);
			this.nombreTextBox.TabIndex = 2;
			// 
			// autorTextBox
			// 
			this.autorTextBox.Location = new System.Drawing.Point(126, 113);
			this.autorTextBox.Name = "autorTextBox";
			this.autorTextBox.Size = new System.Drawing.Size(100, 20);
			this.autorTextBox.TabIndex = 3;
			// 
			// anoTextBox
			// 
			this.anoTextBox.Location = new System.Drawing.Point(126, 152);
			this.anoTextBox.Name = "anoTextBox";
			this.anoTextBox.Size = new System.Drawing.Size(100, 20);
			this.anoTextBox.TabIndex = 4;
			// 
			// edicionTextBox
			// 
			this.edicionTextBox.Location = new System.Drawing.Point(126, 194);
			this.edicionTextBox.Name = "edicionTextBox";
			this.edicionTextBox.Size = new System.Drawing.Size(100, 20);
			this.edicionTextBox.TabIndex = 5;
			// 
			// save
			// 
			this.save.Location = new System.Drawing.Point(32, 243);
			this.save.Name = "save";
			this.save.Size = new System.Drawing.Size(75, 23);
			this.save.TabIndex = 6;
			this.save.Text = "Guardar";
			this.save.UseVisualStyleBackColor = true;
			this.save.Click += new System.EventHandler(this.SaveClick);
			// 
			// search
			// 
			this.search.Location = new System.Drawing.Point(113, 243);
			this.search.Name = "search";
			this.search.Size = new System.Drawing.Size(75, 23);
			this.search.TabIndex = 7;
			this.search.Text = "Buscar";
			this.search.UseVisualStyleBackColor = true;
			this.search.Click += new System.EventHandler(this.SearchClick);
			// 
			// update
			// 
			this.update.Location = new System.Drawing.Point(194, 243);
			this.update.Name = "update";
			this.update.Size = new System.Drawing.Size(75, 23);
			this.update.TabIndex = 8;
			this.update.Text = "Actualizar";
			this.update.UseVisualStyleBackColor = true;
			this.update.Click += new System.EventHandler(this.UpdateClick);
			// 
			// cerrarDatos
			// 
			this.cerrarDatos.Location = new System.Drawing.Point(811, 351);
			this.cerrarDatos.Name = "cerrarDatos";
			this.cerrarDatos.Size = new System.Drawing.Size(75, 23);
			this.cerrarDatos.TabIndex = 9;
			this.cerrarDatos.Text = "cerrar";
			this.cerrarDatos.UseVisualStyleBackColor = true;
			this.cerrarDatos.Click += new System.EventHandler(this.CerrarDatosClick);
			// 
			// dataGridView1
			// 
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.C1,
			this.C2,
			this.C3,
			this.C4,
			this.C5});
			this.dataGridView1.Location = new System.Drawing.Point(341, 30);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(545, 289);
			this.dataGridView1.TabIndex = 10;
			this.dataGridView1.DoubleClick += new System.EventHandler(this.DataGridView1DoubleClick);
			// 
			// C1
			// 
			this.C1.HeaderText = "ID";
			this.C1.Name = "C1";
			// 
			// C2
			// 
			this.C2.HeaderText = "Nombre";
			this.C2.Name = "C2";
			// 
			// C3
			// 
			this.C3.HeaderText = "Autor";
			this.C3.Name = "C3";
			// 
			// C4
			// 
			this.C4.HeaderText = "Año";
			this.C4.Name = "C4";
			// 
			// C5
			// 
			this.C5.HeaderText = "Edicion";
			this.C5.Name = "C5";
			// 
			// mostrar
			// 
			this.mostrar.Location = new System.Drawing.Point(73, 272);
			this.mostrar.Name = "mostrar";
			this.mostrar.Size = new System.Drawing.Size(75, 23);
			this.mostrar.TabIndex = 11;
			this.mostrar.Text = "Mostrar";
			this.mostrar.UseVisualStyleBackColor = true;
			this.mostrar.Click += new System.EventHandler(this.MostrarClick);
			// 
			// Borrar
			// 
			this.Borrar.Location = new System.Drawing.Point(154, 272);
			this.Borrar.Name = "Borrar";
			this.Borrar.Size = new System.Drawing.Size(75, 23);
			this.Borrar.TabIndex = 12;
			this.Borrar.Text = "Borrar";
			this.Borrar.UseVisualStyleBackColor = true;
			this.Borrar.Click += new System.EventHandler(this.BorrarClick);
			// 
			// data
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(914, 420);
			this.Controls.Add(this.Borrar);
			this.Controls.Add(this.mostrar);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.cerrarDatos);
			this.Controls.Add(this.update);
			this.Controls.Add(this.search);
			this.Controls.Add(this.save);
			this.Controls.Add(this.edicionTextBox);
			this.Controls.Add(this.anoTextBox);
			this.Controls.Add(this.autorTextBox);
			this.Controls.Add(this.nombreTextBox);
			this.Controls.Add(this.IdTextBox);
			this.Controls.Add(this.Edicion);
			this.Controls.Add(this.Ano);
			this.Controls.Add(this.Autor);
			this.Controls.Add(this.Nombre);
			this.Controls.Add(this.ID);
			this.Name = "data";
			this.Text = "data";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
