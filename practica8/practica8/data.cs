﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 07/03/2017
 * Time: 10:20 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace practica8
{
	/// <summary>
	/// Description of data.
	/// </summary>
	public partial class data : Form
	{
		public data()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		public MySqlConnection dbconn()
		{
			return new MySqlConnection("server=localhost; database=baseprogramacion; uid=root; pwd=pasito perron");
		}
		
		void CerrarDatosClick(object sender, EventArgs e)
		{
			MainForm principal = new MainForm();
			this.Close();
			principal.Show();
		}
		void SaveClick(object sender, EventArgs e)
		{
			using (MySqlConnection cnn = dbconn())
			{
				cnn.Open();
			 	MySqlCommand cmd = cnn.CreateCommand();
				cmd.CommandType= System.Data.CommandType.Text;
				cmd.CommandText= "insert into books (NombreDeLibro, autor, ano, edicion) values ('"+nombreTextBox.Text+"', '"+autorTextBox.Text+"', '"+anoTextBox.Text+"', '"+edicionTextBox.Text+"')";			
				cmd.ExecuteNonQuery();
				cnn.Close();
			}
			mostrarTabla();
			resetear();
		}
		void SearchClick(object sender, EventArgs e)
		{
			using(MySqlConnection cnn = dbconn())
			{
				cnn.Open();
				MySqlCommand cmd = cnn.CreateCommand();
				cmd.CommandType= System.Data.CommandType.Text;
				//cmd.CommandText= "select * from books where id like '"+IdTextBox.Text+"' ";
//				cmd.CommandText= "select * from books where id = '"+IdTextBox.Text+"' or NombreDeLibro like '%"+nombreTextBox.Text+"%' or autor = '"+autorTextBox.Text+"' or ano = '"+anoTextBox.Text+"' or edicion = '"+edicionTextBox.Text+"'    ";
				cmd.CommandText= "select * from books where id = '"+IdTextBox.Text+"' or NombreDeLibro = '"+nombreTextBox.Text+"' or autor = '"+autorTextBox.Text+"' or ano = '"+anoTextBox.Text+"' or edicion = '"+edicionTextBox.Text+"'    ";
				MySqlDataReader leer;
				leer = cmd.ExecuteReader();
				this.dataGridView1.Rows.Clear();
				while(leer.Read())
				{
					this.dataGridView1.Rows.Add(leer.GetValue(0),leer.GetValue(1),leer.GetValue(2),leer.GetValue(3),leer.GetValue(4));
				}
				cnn.Close();
				resetear();
			}
		}
		
		void UpdateClick(object sender, EventArgs e)
		{
			using(MySqlConnection cnn = dbconn())
			{
				cnn.Open();
				MySqlCommand cmd = cnn.CreateCommand();
				cmd.CommandType= System.Data.CommandType.Text;
				cmd.CommandText= "update books set NombreDeLibro = '"+nombreTextBox.Text+"' where id = '"+IdTextBox.Text+"'";			
				cmd.ExecuteNonQuery();
				cnn.Close();
			}
			mostrarTabla();
			resetear();
		}
		
		void DataGridView1DoubleClick(object sender, EventArgs e)
		{
			IdTextBox.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
			nombreTextBox.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
			autorTextBox.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
			anoTextBox.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
			edicionTextBox.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
		}
		void MostrarClick(object sender, EventArgs e)
		{
			mostrarTabla();
		}
		void BorrarClick(object sender, EventArgs e)
		{
			using (MySqlConnection cnn = dbconn())
			{
				cnn.Open();
				MySqlCommand cmd = cnn.CreateCommand();
				cmd.CommandType= System.Data.CommandType.Text;
				//refresh the table
				cmd.CommandText = "delete from books where id ='"+IdTextBox.Text+"'";
				cmd.ExecuteNonQuery();
				cnn.Close();
			}
			mostrarTabla();
			resetear();
		}
		void resetear()
		{
			IdTextBox.Text = "";
			nombreTextBox.Text = "";
			autorTextBox.Text = "";
			anoTextBox.Text = "";
			edicionTextBox.Text = "";
		}
		void mostrarTabla()
		{
			using(MySqlConnection cnn = dbconn())
			{
				cnn.Open();
				MySqlCommand cmd = cnn.CreateCommand();
				cmd.CommandType= System.Data.CommandType.Text;
				//refresh the table
				cmd.CommandText = "select * from books";
				MySqlDataReader leer;
				leer = cmd.ExecuteReader();
				this.dataGridView1.Rows.Clear();
				while(leer.Read())
				{
					this.dataGridView1.Rows.Add(leer.GetValue(0),leer.GetValue(1),leer.GetValue(2),leer.GetValue(3),leer.GetValue(4));
				}
				cnn.Close();
			}
		}
	}
}