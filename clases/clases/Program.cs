﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 30/01/2017
 * Time: 03:31 p.m.}
 *
 * uso de clases
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace clases
{
	class Program
	{
		public static void Main(string[] args)
		{
			Car car;
			
			car = new Car("Red");
			Console.WriteLine(car.Describe());
			
			car = new Car("Green");
			Console.WriteLine(car.Describe());
			
			Console.ReadLine();
			
		}
	}
	class Car
	{
		private string color;
		
		public Car(string color)
		{
			this.color = color;
		}
		
		public string Describe()
		{
			return "This car  is " + Color;
		}
		
		public string Color
		{
			get
			{
				return color;
			}
			set
			{
				color = value;
			}
		}
		
	}
}