﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 19/01/2017
 * Time: 11:45 a.m.
 * 
 * Metodos
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace funciones
{
	class Program
	{
		public static void Main(string[] args)
		{
			//int number = 20;
	    	//AddFive(ref number);
	   	 	//Console.WriteLine(number);
	   	 	GreetPersons(0);
	   	 	GreetPersons(25,"Juan","Felipe","Manuel","Gerardo");
	   		Console.ReadKey();
		}
		static void AddFive(ref int number)
		{
		    number = number + 5;
		}
		//Funcion para saludar a toda una lista de personas
		static void GreetPersons(int sUP/*No usada*/, params string[] names)
		{
			foreach(string name in names)
			{
				Console.WriteLine("Hello " + name);
			}
		}
	}
}