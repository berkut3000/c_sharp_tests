﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 30/01/2017
 * Time: 03:04 p.m.
 * 
 * arreglos
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Arrays
{
	class Program
	{
		public static void stringss()
		{
			string[] names = new string[2];
			names[0] = "John Doe";
			names[1] = "Jane Doe";
			
			foreach(string s in names)
				Console.WriteLine(s);
			for(int i = 0; i<names.Length;i++)
			{
				Console.WriteLine("Item Number " + i + ":" + names[i]);
			}
			
			Console.ReadLine();
		}
		public static void numberss()
		{
			int[] numbers = new int[5] {4, 3, 8, 0, 5};
			Array.Sort(numbers);
			
			foreach(int i  in numbers)
				Console.WriteLine(i);
			
			Console.ReadLine();
			
		}
			
		public static void Main()
		{
				//stringss();
				numberss();
			
		}
	}
}