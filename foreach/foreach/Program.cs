﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 18/01/2017
 * Time: 03:22 p.m.
 * Ciclo ForEach
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;

namespace forEach
{
	class Program
	{
		public static void Main(string[] args)
		{
			//string name;
			ArrayList lista = new ArrayList();
			lista.Add("Juan mendez");
			lista.Add("Miguel Dominguez");
			lista.Add("Pedro alex");
			
			foreach(string name in lista)
			Console.WriteLine(name);
			
			Console.ReadKey(true);
		}
	}
}