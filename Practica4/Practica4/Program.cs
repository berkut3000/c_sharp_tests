﻿/*
 * Created by Antonio.
 * User: hydro
 * Date: 18/01/2017
 * Time: 08:38 a.m.
 * Ciclos For
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Practica4
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Programa para ciclos for");
			Console.WriteLine("Programa para incrementar en 1 hasta 100");
			for (int h = 0; h<100;h++)
			{
				Console.Write("{0}, ",h+1);
			}
			Console.ReadKey(true);
			Console.WriteLine("\nPrograma para incrementar en 2 hasta 100");
			for(int m = 2; m<=100;m += 2)
			{
				Console.Write("{0},", m);
			}
			Console.ReadKey(true);
			Console.WriteLine("\nPrograma para incrementar un valor,\ny el numero de iteraciones en que se incrementa dicho valor");
			for(int i=1;i<=20;i++)
			{
				for(int j=1;j<=i;j++)
				{
					Console.Write("{0} ",j);
				}
				Console.WriteLine();
			}
			 	
			// TODO: Implement Functionality Here
			
			Console.Write("Ya acabé, wey");
			Console.ReadKey(true);
		}
	}
}