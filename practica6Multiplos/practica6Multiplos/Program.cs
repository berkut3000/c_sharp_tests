﻿/*
 * Created by Antonio.
 * User: hydro
 * Date: 18/01/2017
 * Time: 09:36 a.m.
 * Programa para determinar multiplos de 3 y 5
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace practica6Multiplos
{
	class Program
	{
		public static void Main(string[] args)
		{
			int a,b,c,m,n,y = 0,z = 0;
			//y=0;
			Console.WriteLine("Ingresar cantidad de números a ingresar");
			m = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Ingresar números");
			for(n = 1;n<=m;n++)
			{
				a = Convert.ToInt32(Console.ReadLine());
				b=a%3;//Multiplo de 3
				c=a%5;//Múltiplo de 5
				if(b == 0)
				{
					y++;//Incrementa si el número es multiplo de 3
				}		
				 if(c == 0)
				{
					z++;//Incrementa si el número es múltiplo de 5
				}	
			}
			Console.WriteLine("Multiplos de 3: {0} , Multiplos de 5: {1}",y,z);
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}