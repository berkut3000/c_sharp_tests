﻿/*
 * Created by SharpDevelop.
 * User: hydro
 * Date: 30/01/2017
 * Time: 09:02 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace practica7
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button button1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(33, 73);
			this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(194, 85);
			this.label1.TabIndex = 0;
			this.label1.Text = "Nombre:";
			this.label1.Click += new System.EventHandler(this.Label1Click);
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.textBox1.Location = new System.Drawing.Point(250, 70);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(276, 34);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "Ingrese el nombre";
			this.textBox1.TextChanged += new System.EventHandler(this.TextBox1TextChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(44, 110);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(166, 23);
			this.label2.TabIndex = 2;
			this.label2.Text = "Apellido Paterno";
			// 
			// textBox2
			// 
			this.textBox2.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.textBox2.Location = new System.Drawing.Point(250, 109);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(276, 34);
			this.textBox2.TabIndex = 2;
			this.textBox2.Text = "Ingrese el apellido paterno";
			this.textBox2.TextChanged += new System.EventHandler(this.TextBox2TextChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(33, 150);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(177, 23);
			this.label3.TabIndex = 2;
			this.label3.Text = "Apellido Materno";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(33, 113);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(166, 23);
			this.label4.TabIndex = 2;
			this.label4.Text = "Apellido Paterno";
			// 
			// textBox3
			// 
			this.textBox3.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.textBox3.Location = new System.Drawing.Point(250, 146);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(276, 34);
			this.textBox3.TabIndex = 3;
			this.textBox3.Text = "Ingrese el apellido materno";
			this.textBox3.TextChanged += new System.EventHandler(this.TextBox2TextChanged);
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dateTimePicker1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePicker1.Location = new System.Drawing.Point(254, 186);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(272, 34);
			this.dateTimePicker1.TabIndex = 4;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(33, 186);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(207, 23);
			this.label5.TabIndex = 5;
			this.label5.Text = "Fecha de nacimiento";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(432, 313);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(98, 67);
			this.button1.TabIndex = 6;
			this.button1.Text = "Cerrar";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(568, 544);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.dateTimePicker1);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(6);
			this.Name = "MainForm";
			this.Text = "practica7";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
